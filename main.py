"""
Main script to compute the primary aberrations.
"""
from aberrations import OvoidAberrations

obj = OvoidAberrations(
    wavelength=0.58756,
    exit_pupil_radius=10,
    exit_pupil_position=95,
    stigmatic_object_position=0,
    stigmatic_image_position=265.609311672791,
    surface_position=80,
    maximum_object_height=15,
    object_space_refractive_index=1.0,
    image_space_refractive_index=1.5168001097398938,
    object_position_shift=0
)

# print result information
obj.print_info()
# plot result
obj.plot()
