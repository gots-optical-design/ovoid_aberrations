"""
Visualization functions used with matplotlib.
"""
import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np


def shifted_color_map(cmap, start=0, midpoint=0.5, stop=1.0, name="shiftedcmap"):
    """
    Function to offset the "center" of a colormap.
    """
    cdict = {
        "red": [],
        "green": [],
        "blue": [],
        "alpha": []
    }

    reg_index = np.linspace(start, stop, 257)

    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict["red"].append((si, r, r))
        cdict["green"].append((si, g, g))
        cdict["blue"].append((si, b, b))
        cdict["alpha"].append((si, a, a))

    newcmap = colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap
