"""
Visualization functions used with matplotlib.
"""
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from mpl_toolkits.mplot3d.axes3d import Axes3D
import numpy as np

from functions import shifted_color_map


class OvoidAberrations:
    """
    :class:`OvoidAberrations` class to compute the primary aberrations present in Ovoid surfaces.
    """
    def __init__(self, **kwargs) -> None:
        """
        OvoidAberrations class to compute the primary aberrations present in Ovoid surfaces.
        :param kwargs:
            The keyword arguments for the OvoidAberrations class. The following keys are supported:
            * *wavelength* (``float``) --
                The wavelength of the light, default 0.58756 micrometers.
            * *exit_pupil_radius* (``float``) --
                The radius of the exit pupil, default None.
            * *exit_pupil_position* (``float``) --
                The position of the exit pupil, default None.
            * *stigmatic_object_position* (``float``) --
                The position of the stigmatic object, default None.
            * *stigmatic_image_position* (``float``) --
                The position of the stigmatic image, default None.
            * *surface_position* (``float``) --
                The position of the vertex of the surface, default None.
            * *maximum_object_height* (``float``) --
                The maximum height of the object, default None.
            * *object_space_refractive_index* (``float``) --
                The refractive index of the object space, default None.
            * *image_space_refractive_index* (``float``) --
                The refractive index of the image space, default None.
            * *object_position_shift* (``float``) --
                The shift of the object position, default None. All the other parameters
                are used to define the Cartesian ovoid but the shift is applied to set
                the vertex position of the surface and the pupil positions to determine
                the aberrations. So, after calculating the form parameters, the shift is
                applied as follows:

                ``surface_position = surface_position - object_position_shift``

                ``exit_pupil_position = exit_pupil_position - object_position_shift``
        :return:
        """
        # set all the parameters
        self.wavelength: float = kwargs.get("wavelength", 0.58756)
        self.epr: float = kwargs.get("exit_pupil_radius")
        self.epp: float = kwargs.get("exit_pupil_position")
        self.zeta: float = kwargs.get("surface_position")
        self.d0: float = kwargs.get("stigmatic_object_position")
        self.d1: float = kwargs.get("stigmatic_image_position")
        self.maximum_object_height: float = kwargs.get("maximum_object_height")
        self.object_fields: list = [self.maximum_object_height, 0.7 * self.maximum_object_height, 0]
        self.n0: float = kwargs.get("object_space_refractive_index")
        self.n1: float = kwargs.get("image_space_refractive_index")
        self.shift: float = kwargs.get("object_position_shift", 0)
        # calculate the form parameters of the ovoid surface
        self.g_param, self.o_param, self.s_param = self.__calculate_form_parameters()
        # calculate the primary aberrations
        self.a_sq, self.a_cq, self.a_aq, self.a_dq, self.a_tq = self.__calculate_aberrations()
        # calculate the aspherical coefficients from 4th to 10th order
        self.a4, self.a6, self.a8, self.a10 = self.__calculate_aspherical_coefficients()

    def __calculate_aberrations(self) -> tuple:
        """
        Method to calculate the primary aberrations.
        :return: tuple
        """
        # object position
        s0 = self.d0
        # set surface position according to the object position shift
        self.zeta = self.zeta - self.shift
        # set the pupil position according to the object position shift
        self.epp = self.epp - self.shift
        # image position
        s1 = self.zeta + 1 / ((self.n0 / self.n1) * (1 / (s0 - self.zeta) - self.o_param) + self.o_param)
        # parameter b0
        b0 = (self.zeta + 1 / self.o_param - self.epp) / (s1 - self.zeta - 1 / self.o_param)
        # parameter g0
        g0 = (self.epp - self.zeta) / (s1 - self.epp)
        # magnification
        self.magnification = (self.n0 / self.n1) * (s1 - self.zeta) / (s0 - self.zeta)
        # if s_param is 0, alpha_0 is computed using an expression that avoid indeterminacies
        if self.s_param == 0:
            alpha_0 = 0.125 * (self.n1 - self.n0) * self.o_param ** 3 * self.g_param
        else:
            alpha_0 = -0.125 * (self.n1 - self.n0) * (
                    2 * self.s_param - self.o_param ** 2 * self.g_param
            ) ** 2 / (self.g_param * self.o_param)

        # spherical aberration coefficient is numerically negative for s0 - zeta < 0 y s1 - zeta > 0,
        # in these cases the aberration coefficient is positive.
        a_s = -0.125 * self.n1 ** 2 * (1 / (s1 - self.zeta) - self.o_param) ** 2 * (
                1 / (self.n1 * (s1 - self.zeta)) - 1 / (self.n0 * (s0 - self.zeta)))

        # Gauss curvature
        image_curvature = ((self.n0 - self.n1) / self.n0) * self.o_param
        # Bonnet curvature
        image_curvature = -((s0 - self.zeta) / (s1 - self.zeta)) * image_curvature

        # spherical aberration coefficient that include the effect of exit pupil position
        a_ss = ((s1 - self.zeta) / (s1 - self.epp)) ** 4 * a_s
        # spherical aberration for ovoid surface
        a_sq = a_ss + ((s1 - self.zeta) / (s1 - self.epp)) ** 4 * alpha_0
        # coma aberration for ovoid surface
        a_cq = 4 * (a_ss * b0 - ((s1 - self.zeta) / (s1 - self.epp)) ** 3 * alpha_0 * g0)
        # astigmatism for ovoid surface
        a_aq = 4 * (a_ss * b0 ** 2 + ((s1 - self.zeta) / (s1 - self.epp)) ** 2 * alpha_0 * g0 ** 2)
        # field curvature for ovoid surface
        a_dq = 0.5 * a_aq + 0.25 * self.n1 * image_curvature / (s1 - self.epp) ** 2
        # distortion for ovoid surface
        a_tq = 4 * (a_ss * b0 ** 3 - ((s1 - self.zeta) / (s1 - self.epp)) * alpha_0 * g0 ** 3) + (
                    self.n1 * image_curvature / (2 * (s1 - self.epp))
        ) * (1 / (s1 - 1 / self.o_param - self.zeta) - 1 / (s1 - self.epp))
        # normalization of the aberrations coefficients
        a_sq = -a_sq * self.epr ** 4 / (self.wavelength * 0.001)
        a_cq = -a_cq * self.magnification * self.maximum_object_height * self.epr ** 3 / (self.wavelength * 0.001)
        a_aq = -a_aq * (
                self.magnification * self.maximum_object_height) ** 2 * self.epr ** 2 / (self.wavelength * 0.001)
        a_dq = -a_dq * (
                self.magnification * self.maximum_object_height) ** 2 * self.epr ** 2 / (self.wavelength * 0.001)
        a_tq = -a_tq * (self.magnification * self.maximum_object_height) ** 3 * self.epr / (self.wavelength * 0.001)

        return a_sq, a_cq, a_aq, a_dq, a_tq

    def __calculate_form_parameters(self) -> tuple:
        """
        Method to calculate the form parameters of the ovoid surface, G, O, and S.
        :return: tuple
        """
        # G parameter
        g_param = (self.n1 ** 2 / (self.d0 - self.zeta) - self.n0 ** 2 / (self.d1 - self.zeta)) ** 2 / (
                self.n0 * self.n1 * (self.n1 / (self.d1 - self.zeta) - self.n0 / (self.d0 - self.zeta)) * (
                    self.n1 / (self.d0 - self.zeta) - self.n0 / (self.d1 - self.zeta)))
        # O parameter
        o_param = (self.n1 / (self.d1 - self.zeta) - self.n0 / (self.d0 - self.zeta)) / (self.n1 - self.n0)
        # S parameter
        s_param = (self.n0 + self.n1) * (
                -self.n0 ** 2 / (self.d1 - self.zeta) + self.n1 ** 2 / (self.d0 - self.zeta)) / (
                2 * self.n0 * self.n1 * (self.d0 - self.zeta) * (self.d1 - self.zeta) * (
                    -self.n0 / (self.d1 - self.zeta) + self.n1 / (self.d0 - self.zeta)))
        return g_param, o_param, s_param

    def __calculate_aspherical_coefficients(self) -> tuple:
        """
        Method to calculate the aspherical coefficients from 4th to 10th order.
        :return: tuple
        """
        # fourth order coefficient
        a4 = -self.s_param * (self.g_param * self.o_param ** 2 - self.s_param) / (2 * self.g_param * self.o_param)
        # sixth order coefficient
        a6 = self.s_param * (
                self.g_param * self.o_param ** 2 * (
                    -3 * self.g_param * self.o_param ** 2 - 4 * self.o_param ** 2 + 6 * self.s_param
                ) + 4 * self.o_param ** 2 * self.s_param - 4 * self.s_param ** 2) / (8 * self.g_param * self.o_param)
        # eighth order coefficient
        a8 = 5 * self.s_param * (
                self.g_param ** 2 * self.o_param ** 2 * (
                    -2 * self.g_param ** 2 * self.o_param ** 4 - 5 * self.g_param * self.o_param ** 4
                    + 6 * self.g_param * self.o_param ** 2 * self.s_param - 3 * self.o_param ** 4
                    + 12 * self.o_param ** 2 * self.s_param - 8 * self.s_param ** 2
                ) + 3 * self.g_param * self.o_param ** 2 * self.s_param * (
                    self.o_param ** 2 - 4 * self.s_param
                ) + 4 * self.g_param * self.s_param ** 3 + 4 * self.s_param ** 3
        ) / (32 * self.g_param ** 2 * self.o_param)
        # tenth order coefficient
        a10 = self.s_param * (self.g_param ** 3 * self.o_param ** 4 * (
                -35 * self.g_param ** 3 * self.o_param ** 6 - 126 * self.g_param ** 2 * self.o_param ** 6
                + 140 * self.g_param ** 2 * self.o_param ** 4 * self.s_param - 147 * self.g_param * self.o_param ** 6
                + 450 * self.g_param * self.o_param ** 4 * self.s_param
                - 280 * self.g_param * self.o_param ** 2 * self.s_param ** 2 - 56 * self.o_param ** 6
                + 378 * self.o_param ** 4 * self.s_param - 800 * self.o_param ** 2 * self.s_param ** 2
                + 280 * self.s_param ** 3) + 4 * self.g_param ** 2 * self.o_param ** 4 * self.s_param * (
                    14 * self.o_param ** 4 - 105 * self.o_param ** 2 * self.s_param + 180 * self.s_param ** 2
                ) - 112 * self.g_param ** 2 * self.o_param ** 2 * self.s_param ** 4
                    + 168 * self.g_param * self.o_param ** 4 * self.s_param ** 3
                    - 288 * self.g_param * self.o_param ** 2 * self.s_param ** 4 + 32 * self.s_param ** 5
                ) / (128 * self.g_param ** 3 * self.o_param ** 3)
        return a4, a6, a8, a10

    def plot(self) -> None:
        """
        Method to plot the aberrations for the ovoid surface.
        """
        # plots container
        container = []
        # radial coordinate
        radial_coord = np.linspace(0, self.epr, 24)
        # angular coordinate
        angular_coord = np.linspace(0, 2 * np.pi, 72)
        # r coordinate on the pupil plane and the azimuthal coordinate
        r, theta = np.meshgrid(radial_coord, angular_coord)
        # from polar to cartesian
        x, y = r * np.cos(theta), r * np.sin(theta)
        min_field, max_field = 99999, 0
        # for each field in the object are calculated the aberrations
        for h0 in self.object_fields:
            # image height
            h1 = self.magnification * h0
            # spherical aberrations
            ws = self.a_sq * (r / self.epr) ** 4
            # coma
            wc = self.a_cq * (h1 / (self.magnification * self.maximum_object_height)
                              ) * (r / self.epr) ** 3 * np.cos(theta)
            # astigmatism
            wa = self.a_aq * (h1 / (self.magnification * self.maximum_object_height)
                              ) ** 2 * (r / self.epr) ** 2 * (np.cos(theta)) ** 2
            # field curvature
            wd = self.a_dq * (h1 / (self.magnification * self.maximum_object_height)
                              ) ** 2 * (r / self.epr) ** 2
            # distortion
            wt = self.a_tq * (h1 / (self.magnification * self.maximum_object_height)
                              ) ** 3 * (r / self.epr) * np.cos(theta)
            # total aberration
            w = ws + wc + wa + wd + wt
            # add exit pupil wavefront field data to container
            # add spherical aberrations
            container.append({"data": ws, "label": f"Spherical aberration, Field={h0}"})
            # add coma
            container.append({"data": wc, "label": f"Coma, Field={h0}"})
            # add astigmatism
            container.append({"data": wa, "label": f"Astigmatism, Field={h0}"})
            # add field curvature
            container.append({"data": wd, "label": f"Field curvature, Field={h0}"})
            # add distortion
            container.append({"data": wt, "label": f"Distortion, Field={h0}"})
            # add total aberration
            container.append({"data": w, "label": f"Total aberration, Field={h0}"})

        # create array of plots container using matplotlib
        fig, axs = plt.subplots(3, 6, figsize=(12, 6), layout="constrained")
        # create a shifted color map
        cmap = shifted_color_map(cm.turbo, midpoint=0.6, name="shifted")
        i = 0
        # plot each exit pupil wavefront field
        for ax in axs.flat:
            # get the field data from the container
            data = container[i]["data"]
            label = container[i]["label"]
            # normalize the color
            norm = colors.Normalize(vmin=np.min(data), vmax=np.max(data))
            # set the keywords arguments to be passed to pcolormesh
            pc_kwargs = {"rasterized": True, "cmap": cmap, "norm": norm, "shading": "gouraud"}
            # plot the exit pupil wavefront field
            im = ax.pcolormesh(y, x, data, **pc_kwargs)
            ax.set_title(label)
            fig.colorbar(im, ax=ax, fraction=0.046, pad=0.04, shrink=0.6)
            ax.set_aspect("equal")
            i += 1

        plt.show()

    def print_info(self):
        print("System parameters:")
        print("surface position: ", self.zeta)
        print("exit pupil position: ", self.epp)
        print("exit pupil radius: ", self.epr)
        print("wavelength: ", self.wavelength)
        print("maximum object height: ", self.maximum_object_height)
        print("object position: ", self.d0)
        print("object space refractive index: ", self.n0)
        print("image space refractive index: ", self.n1)

        print("Ovoid form parameters:")
        print("o_param: ", self.o_param)
        print("s_param: ", self.s_param)
        print("g_param: ", self.g_param)

        print("Ovoid aspherical coefficients:")
        print("a4: ", self.a4)
        print("a6: ", self.a6)
        print("a8: ", self.a8)
        print("a10: ", self.a10)

        print("Primary aberrations coefficients:")
        print("a_sq: ", self.a_sq)
        print("a_cq: ", self.a_cq)
        print("a_aq: ", self.a_aq)
        print("a_dq: ", self.a_dq)
        print("a_tq: ", self.a_tq)
