# Ovoid Aberrations



## Getting started

This code can calculate the primary aberrations coefficients of a Cartesian surface or Descartes ovoid. Also, can plot the aberrated field on the exit pupil for three different positions on the object field. The plot is ordered in a matrix of plots with three rows and six columns, where the rows represent the field positions (up to down from maximum field position to on-axis), and the columns represent the aberrations (from left to right spherical aberration, coma, astigmatism, field curvature, distortion, the sum of the primary aberrations). The calculation process is derived from the following published manuscripts:

- [1] [Silva-Lora, A., & Torres, R. (2020). Superconical aplanatic ovoid singlet lenses. JOSA A, 37(7), 1155-1165.](https://doi.org/10.1364/JOSAA.392795)
- [2] [Silva-Lora, A., & Torres, R. (2020). Explicit Cartesian oval as a superconic surface for stigmatic imaging optical systems with real or virtual source or image. Proceedings of the Royal Society A, 476(2235), 20190894.](https://doi.org/10.1098/rspa.2019.0894)
- [3] [Silva-Lora, A., & Torres, R. (2023). Stigmatic aspherical refracting surfaces from Cartesian ovoids. JOSA A, 40(4), C30-C36.](https://doi.org/10.1364/JOSAA.482669)

## Requirements

To use these python scripts are needed the following libraries:

- numpy (version==1.25.0)
- matplotlib (version==3.7.1)

Although these are the versions of the libraries used, the execution of the python scripts can work with other versions.

These requirements can be installed using ``pip`` command by executing:

> pip install -r requirements.txt


## Repository content

The repository contains four principal files explained below:
- ``aberrations.py``: This file contains the ``OvoidAberrations`` class which has the instructions to calculate the primary aberrations, and the aspherical coefficients of Ovoid surfaces. After instantiating this class, it is called the ``print_info`` method to visualize all the information of the optical system composed by a single surface. If it is needed to obtain a plot of the aberrated field on the exit pupil, it is used the ``plot`` method which uses ``matplotlib`` to perform this task. An example of a generated plot can be found in the root of this repository as ``example_plot.pdf``.
- ``main.py``: This is an example of how to use the ``OvoidAberrations`` class. In this file it is instantiated the ``OvoidAberrations`` class, and are implemented the ``print_info`` and ``plot`` methods.
- ``functions.py``: Includes a function to shift the color map. This function is used by the ``plot`` method of the ``OvoidAberrations`` due to visualization issue.